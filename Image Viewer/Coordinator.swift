//
//  Coordinator.swift
//  Image Viewer
//
//  Created by David van Enckevort on 28-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import Foundation
import UIKit

class Coordinator: NSObject {
    private let rootViewController: UIPageViewController
    private var imageViewControllers: [UIViewController]
    private var currentImage: Int

    override init() {
        rootViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: [:])
        imageViewControllers = [UIViewController]()
        imageViewControllers.append(ImageViewController(with: #imageLiteral(resourceName: "acropolis")))
        imageViewControllers.append(ImageViewController(with: #imageLiteral(resourceName: "colosseum")))
        imageViewControllers.append(ImageViewController(with: #imageLiteral(resourceName: "deer")))
        imageViewControllers.append(ImageViewController(with: #imageLiteral(resourceName: "flower")))
        imageViewControllers.append(ImageViewController(with: #imageLiteral(resourceName: "langnek")))
        rootViewController.setViewControllers([imageViewControllers.first!], direction: .forward, animated: false, completion: nil)
        currentImage = 0
        super.init()

        rootViewController.dataSource = self
    }

    func start() -> UIViewController {
        return rootViewController
    }
}

extension Coordinator: UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard var index = imageViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        index -= 1
        return index >= 0 ? imageViewControllers[index] : nil
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard var index = imageViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        index += 1
        return index < imageViewControllers.count ? imageViewControllers[index] : nil

    }

    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return imageViewControllers.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentImage
    }

}
