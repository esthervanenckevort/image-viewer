//
//  ViewController.swift
//  Image Viewer
//
//  Created by David van Enckevort on 28-06-18.
//  Copyright © 2018 All Things Digital. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    private var imageView = UIImageView()
    private let scrollView = UIScrollView()
    private var image: UIImage {
        set {
            imageView.image = newValue
            imageView.sizeToFit()
            scrollView.contentSize = imageView.frame.size
        }
        get {
            return imageView.image!
        }
    }

    init(with image: UIImage) {
        super.init(nibName: nil, bundle: nil)
        self.image = image
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let zoomScale = min(view.frame.size.width / image.size.width, view.frame.size.height / image.size.height)
        scrollView.minimumZoomScale = zoomScale
        scrollView.maximumZoomScale = 1.0
        if scrollView.zoomScale < scrollView.minimumZoomScale {
            scrollView.setZoomScale(zoomScale, animated: true)
        }
        if scrollView.zoomScale > scrollView.maximumZoomScale {
            scrollView.setZoomScale(1.0, animated: true)
        }
        setInsets()
    }

    override func loadView() {
        view = scrollView
        scrollView.delegate = self
        scrollView.addSubview(imageView)
        scrollView.minimumZoomScale = 0.0
        scrollView.setZoomScale(0, animated: false)
        scrollView.maximumZoomScale = 1.0
    }

    private func setInsets() {
        if scrollView.contentSize.height < self.view.frame.size.height
            || scrollView.contentSize.width < self.view.frame.size.width {
            let xInset = max(0, (self.view.frame.size.width - scrollView.contentSize.width) / 2)
            let yInset = max(0, (self.view.frame.size.height - scrollView.contentSize.height) / 2)
            scrollView.contentInset = UIEdgeInsets(top: yInset, left: xInset, bottom: yInset, right: xInset)
        } else {
            scrollView.contentInset = UIEdgeInsets()
        }
    }
}

extension ImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        setInsets()
    }
}
